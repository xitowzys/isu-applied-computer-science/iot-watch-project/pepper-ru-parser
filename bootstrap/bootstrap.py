import sys
from .app import start_app
from loguru import logger as log


def logger_configuration() -> None:
    # TODO: Добавить создание папки logs если её нету

    # logger.add("./logs/logs.log", format="({time}) {level} {message}",
    #            level="DEBUG", rotation="10 KB", compression="zip", serialize=True)

    log.remove()

    log.add(
        sys.stdout, colorize=True,
        format="(<level>{level}</level>) [<green>{time:HH:mm:ss}</green>] ➤ <level>{message}</level>")


def bootstrap() -> None:
    """Launching the application
    """
    logger_configuration()
    start_app()
