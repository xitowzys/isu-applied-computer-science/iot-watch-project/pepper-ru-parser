from environs import Env
from werkzeug.security import generate_password_hash

env = Env()
env.read_env()

AUTH_USERNAME: str = env.str("AUTH_USERNAME")
AUTH_PASSWORD = generate_password_hash(env.str("AUTH_PASSWORD"))

MONITORING: bool = env.bool("MONITORING")
TIME_MONITORING: int = env.int("TIME_MONITORING")

FLASK_HOST: str = env.str("FLASK_HOST")
FLASK_PORT: int = env.int("FLASK_PORT")
FLASK_DEBUG: bool = env.bool("FLASK_DEBUG")

DB_HOST: str = env.str("DB_HOST")
DB_PORT: str = env.str("DB_PORT")
DB_DATABASE: str = env.str("DB_DATABASE")
DB_USERNAME: str = env.str("DB_USERNAME")
DB_PASSWORD: str = env.str("DB_PASSWORD")