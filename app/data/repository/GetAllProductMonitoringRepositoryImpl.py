from app.data.storage.IGetAllProductMonitoringDBStorage import IGetAllProductMonitoringDBStorage
from app.domain.repository.IGetAllProductMonitoringRepository import IGetAllProductMonitoringRepository

class GetAllProductMonitoringRepositoryImpl(IGetAllProductMonitoringRepository):

    def __init__(self, get_all_product_monitoring_db_storage: IGetAllProductMonitoringDBStorage):
        self.get_all_product_monitoring_db_storage: IGetAllProductMonitoringDBStorage = get_all_product_monitoring_db_storage

    def get(self) -> dict:
        return self.get_all_product_monitoring_db_storage.get()

