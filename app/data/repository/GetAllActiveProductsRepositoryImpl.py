from app.data.storage.IGetAllActiveProductsDBStorage import IGetAllActiveProductsDBStorage
from app.domain.repository.IGetAllActiveProductsRepository import IGetAllActiveProductsRepository

class GetAllActiveProductsRepositoryImpl(IGetAllActiveProductsRepository):

    def __init__(self, get_all_active_products_db_storage: IGetAllActiveProductsDBStorage):
        self.get_all_active_products_db_storage: IGetAllActiveProductsDBStorage = get_all_active_products_db_storage

    def get(self) -> dict:
        return self.get_all_active_products_db_storage.get()

