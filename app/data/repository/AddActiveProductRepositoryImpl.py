from app.data.storage.IAddActiveProductDBStorage import IAddActiveProductDBStorage
from app.domain.repository.IAddActiveProductRepository import IAddActiveProductRepository
from app.domain.models.ActiveProduct import ActiveProduct as domainModelsActiveProduct
from app.data.storage.models.ActiveProduct import ActiveProduct as storageModelsActiveProduct


class AddActiveProductRepositoryImpl(IAddActiveProductRepository):

    def __init__(self, add_active_product_db_storage: IAddActiveProductDBStorage):
        self.add_active_product_db_storage: IAddActiveProductDBStorage = add_active_product_db_storage

    def add(self, active_product: domainModelsActiveProduct) -> int:
        storage_models_active_product: storageModelsActiveProduct = storageModelsActiveProduct(
            active_product.product_mon_id,
            active_product.product_pepper_id,
            active_product.title,
            active_product.url)

        return self.add_active_product_db_storage.add(storage_models_active_product)
