from abc import ABC, abstractmethod

from app.data.storage.models.ProductName import ProductName


class IAddProductMonitoringDBStorage(ABC):

    @abstractmethod
    def add(self, product_name: ProductName) -> int:
        pass