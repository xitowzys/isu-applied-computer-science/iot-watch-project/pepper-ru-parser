from dataclasses import dataclass


@dataclass
class ActiveProduct:
    product_mon_id: int
    product_pepper_id: int
    title: str
    url: str
