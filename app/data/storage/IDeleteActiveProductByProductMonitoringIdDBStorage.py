from abc import ABC, abstractmethod

from app.domain.models.ProductMonitoringId import ProductMonitoringId


class IDeleteActiveProductByProductMonitoringIdDBStorage(ABC):

    @abstractmethod
    def delete(self, product_id: ProductMonitoringId) -> int:
        pass
