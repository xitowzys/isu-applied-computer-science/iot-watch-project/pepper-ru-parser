import sqlalchemy as sa
from loguru import logger as log
from app.data.storage.models.ActiveProduct import ActiveProduct
from app.data.storage.database.SQLAlchemy.engine import engine
from app.data.storage.database.SQLAlchemy.declarative_base import Base
from app.data.storage.IAddActiveProductDBStorage import IAddActiveProductDBStorage
from sqlalchemy.exc import OperationalError


class AddActiveProductStorage(IAddActiveProductDBStorage):
    def add(self, active_product: ActiveProduct) -> int:
        from app.data.storage.database.models.ActiveProducts import ActiveProducts

        Session = sa.orm.sessionmaker()
        Session.configure(bind=engine)
        session = Session()

        try:
            add_active_product = ActiveProducts(
                product_mon_id=active_product.product_mon_id,
                product_pepper_id=active_product.product_pepper_id,
                title=active_product.title,
                url=active_product.url
            )

            session.add_all([add_active_product])
            session.commit()

            log.success(f"(База данных)➤ Добавлен активный товар: {active_product.title}")

            return 0
        except OperationalError as e:
            log.error(
                f"(База данных)➤ Ошибка добавление активный товар {active_product.title}\n{'-'*100}\n{e}\n{'-'*100}")
            return -1
