import sqlalchemy as sa
from loguru import logger as log
from app.data.storage.models.ProductMonitoringId import ProductMonitoringId
from app.data.storage.database.SQLAlchemy.engine import engine
from app.data.storage.database.SQLAlchemy.declarative_base import Base
from app.data.storage.IDeleteActiveProductByProductMonitoringIdDBStorage import IDeleteActiveProductByProductMonitoringIdDBStorage
from sqlalchemy.exc import OperationalError


class DeleteActiveProductByProductMonitoringIdStorage(IDeleteActiveProductByProductMonitoringIdDBStorage):
    def delete(self, product_id: ProductMonitoringId) -> int:
        from app.data.storage.database.models.ProductMonitoring import ProductMonitoring
        from app.data.storage.database.models.ActiveProducts import ActiveProducts

        Session = sa.orm.sessionmaker()
        Session.configure(bind=engine)
        session = Session()

        try:
            # delete_active_products = session.query(ActiveProducts).where(
            #     ActiveProducts.product_mon_id == product_id.id).all()

            delete_active_products = session.query(ActiveProducts).all()

            for o in delete_active_products:
                session.delete(o)

            session.commit()

            log.success(f"(База данных)➤ Удален активный товар: {product_id.id}")

            return 0
        except Exception as e:
            log.error(
                f"(База данных)➤ Ошибка удаление активного товара {product_id.id}\n{'-'*100}\n{e}\n{'-'*100}")
            return -1
