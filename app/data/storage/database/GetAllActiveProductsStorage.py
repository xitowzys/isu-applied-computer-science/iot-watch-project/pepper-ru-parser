import sqlalchemy as sa
from loguru import logger as log
from app.data.storage.models.ProductName import ProductName
from app.data.storage.database.SQLAlchemy.engine import engine
from app.data.storage.database.SQLAlchemy.declarative_base import Base
from app.data.storage.IGetAllActiveProductsDBStorage import IGetAllActiveProductsDBStorage
from sqlalchemy.exc import OperationalError


class GetAllActiveProductsStorage(IGetAllActiveProductsDBStorage):
    def get(self) -> int:
        from app.data.storage.database.models.ActiveProducts import ActiveProducts

        Session = sa.orm.sessionmaker()
        Session.configure(bind=engine)
        session = Session()

        products = {}
        
        active_products_query = session.query(ActiveProducts).all()

        for i, product in enumerate(active_products_query):
            products[i + 1] = product.title

        # print(products)
        

        return products
        # try:
        #     add_product = ProductMonitoring(title=product_name.title)

        #     session.add_all([add_product])
        #     session.commit()

        #     log.success(f"(База данных)➤ Добавлен товар: {product_name.title}")

        #     return 0
        # except OperationalError as e:
        #     log.error(
        #         f"(База данных)➤ Ошибка добавление товара {product_name.title}\n{'-'*100}\n{e}\n{'-'*100}")
        #     return -1
