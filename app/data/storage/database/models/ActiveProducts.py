import enum
import sqlalchemy as sa
from dataclasses import dataclass
from sqlalchemy.dialects.mysql import INTEGER
from app.data.storage.database.SQLAlchemy.declarative_base import Base
from app.data.storage.database.models.ProductMonitoring import ProductMonitoring

class StatusEnum(enum.Enum):
    active = "ACTIVE"
    completed = "COMPLETED"

@dataclass
class ActiveProducts(Base):
    __tablename__ = 'active_products'

    id = sa.Column(INTEGER(unsigned=True),
                   primary_key=True, autoincrement=True)
    product_mon_id = sa.Column(INTEGER(unsigned=True),
                           sa.ForeignKey(ProductMonitoring.id), nullable=False)

    product_pepper_id = sa.Column(INTEGER(unsigned=True), nullable=False)
    title = sa.Column(sa.VARCHAR(length=255), nullable=False)
    url = sa.Column(sa.VARCHAR(length=2083), nullable=False)
    # status = sa.Column(sa.Enum(StatusEnum), nullable=False)
    # is_deleted = sa.Column(BOOLEAN, nullable=False, default=0)