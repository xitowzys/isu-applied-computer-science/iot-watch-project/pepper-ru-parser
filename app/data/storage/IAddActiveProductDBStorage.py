from abc import ABC, abstractmethod

from app.domain.models.ActiveProduct import ActiveProduct


class IAddActiveProductDBStorage(ABC):

    @abstractmethod
    def add(self, active_product: ActiveProduct) -> int:
        pass
