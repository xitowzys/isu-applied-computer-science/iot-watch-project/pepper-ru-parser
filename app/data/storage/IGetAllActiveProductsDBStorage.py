from abc import ABC, abstractmethod

class IGetAllActiveProductsDBStorage(ABC):

    @abstractmethod
    def get(self) -> int:
        pass