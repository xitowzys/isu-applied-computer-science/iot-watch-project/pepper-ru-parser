from typing import Any, Tuple
import requests
import cfscrape
import asyncio, aiohttp
from loguru import logger as log

class GetPageUseCase():
    async def execute(self, product: str, page: int) -> tuple[str, str] | tuple[None, str]:
        async with aiohttp.ClientSession() as session:
            # scraper = cfscrape.create_scraper()

            try:
                response = await session.get(
                    url="https://www.pepper.ru/search",
                    params={
                        "q": product,
                        "page": str(page),
                        "hide_expired": "true"
                    }
                )

                log.debug(f"[INFO] Обработал страницу.\nТовар: {product}")

                return await response.text(), str(response.status)
            except requests.exceptions.RequestException:
                return None, str('HTTP Request failed')