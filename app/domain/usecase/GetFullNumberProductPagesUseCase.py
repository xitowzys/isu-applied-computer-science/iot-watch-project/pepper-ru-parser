import math
from .GetPageUseCase import GetPageUseCase
from .GetNumberDiscountsUseCase import GetNumberDiscountsUseCase
import asyncio, aiohttp

class GetFullNumberProductPagesUseCase():
    async def execute(self, product: str):
        get_number_discounts_use_case: GetNumberDiscountsUseCase = GetNumberDiscountsUseCase()
        
        get_page_use_case: GetPageUseCase = GetPageUseCase()

        page, status_code = await get_page_use_case.execute(product, 0)
        return math.ceil(get_number_discounts_use_case.execute(page) / 20), status_code
