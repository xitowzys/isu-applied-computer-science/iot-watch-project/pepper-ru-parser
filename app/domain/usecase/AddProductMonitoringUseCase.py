from loguru import logger as log
from app.domain.models.ProductName import ProductName
from app.domain.repository.IAddProductMonitoringRepository import IAddProductMonitoringRepository

class AddProductMonitoringUseCase():
    def __init__(self, add_product_monitoringRepositor: IAddProductMonitoringRepository):
        self.add_product_monitoringRepositor: IAddProductMonitoringRepository = add_product_monitoringRepositor

    def execute(self, product_name: ProductName) -> int:
        log.info("Добавление товара для мониторинга")
        return self.add_product_monitoringRepositor.add(product_name)
