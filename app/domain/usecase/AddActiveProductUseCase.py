from loguru import logger as log
from app.domain.models.ActiveProduct import ActiveProduct
from app.domain.repository.IAddActiveProductRepository import IAddActiveProductRepository

class AddActiveProductUseCase():
    def __init__(self, add_active_product_repository: IAddActiveProductRepository):
        self.add_active_product_repository: IAddActiveProductRepository = add_active_product_repository

    def execute(self, active_product: ActiveProduct) -> int:
        log.info("Добавление товара в активные")
        return self.add_active_product_repository.add(active_product)
