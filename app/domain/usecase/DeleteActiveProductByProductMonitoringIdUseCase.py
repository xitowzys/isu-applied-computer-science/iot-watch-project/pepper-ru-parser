from loguru import logger as log
from app.domain.models.ProductMonitoringId import ProductMonitoringId
from app.domain.repository.IDeleteActiveProductByProductMonitoringIdRepository import IDeleteActiveProductByProductMonitoringIdRepository

class DeleteActiveProductByProductMonitoringIdUseCase:
    def __init__(self, delete_active_product_by_product_monitoring_id_repository: IDeleteActiveProductByProductMonitoringIdRepository):
        self.delete_active_product_by_product_monitoring_id_repository: IDeleteActiveProductByProductMonitoringIdRepository = delete_active_product_by_product_monitoring_id_repository

    def execute(self, product_id: ProductMonitoringId) -> int:
        log.info(f"Удаление активных товаров с id {product_id.id}")
        return self.delete_active_product_by_product_monitoring_id_repository.delete(product_id)