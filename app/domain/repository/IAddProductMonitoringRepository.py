from abc import ABC, abstractmethod

from app.domain.models.ProductName import ProductName


class IAddProductMonitoringRepository(ABC):

    @abstractmethod
    def add(self, product_name: ProductName) -> int:
        pass
