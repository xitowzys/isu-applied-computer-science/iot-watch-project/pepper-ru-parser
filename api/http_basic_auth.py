from config import AUTH_USERNAME, AUTH_PASSWORD
from flask_httpauth import HTTPBasicAuth
from werkzeug.security import check_password_hash

auth = HTTPBasicAuth()

@auth.verify_password
def verify_password(username, password):
    if username == AUTH_USERNAME and check_password_hash(AUTH_PASSWORD, password):
        return True
