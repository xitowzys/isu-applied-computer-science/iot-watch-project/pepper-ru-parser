from flask import request
from api.http_basic_auth import auth
from flask_restful import Resource, abort
from app.di import DomainUsecasesDI
from app.domain.models.ProductName import ProductName
from app.domain.models.ProductMonitoringId import ProductMonitoringId
from app.domain.models.ActiveProduct import ActiveProduct

class AddMonProduct(Resource):

    @auth.login_required
    def post(self):
        args = request.args

        if (len(args) > 1):
            abort(400)

        product = args['product']

        usecases = DomainUsecasesDI()
        product_name = ProductName(product)

        # product_id = ProductMonitoringId(43)
        # active_product = ActiveProduct(12, 32432, "test test", "https://test.com")

        # print(usecases.add_active_product_use_case().execute(active_product))

        result = usecases.add_product_monitoring_use_case().execute(product_name)
        if result == 0:
            return {"success": f"Товар {product} был успешно добавлен"}
        elif result == 1:
            return {"error": f"Товар с таким названием уже существует"}
        else:
            return {"message": "Не удалось добавить товар"}
