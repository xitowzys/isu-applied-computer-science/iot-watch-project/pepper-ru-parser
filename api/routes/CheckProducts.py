from flask import jsonify
from flask_restful import Resource
from app.di import DomainUsecasesDI
from api.http_basic_auth import auth
from app.modules import Translator

class CheckProducts(Resource):

    @auth.login_required
    def get(self):
        usecases = DomainUsecasesDI()
        translator = Translator()

        result = usecases.get_all_active_products_use_case().execute()

        for k, v in result.items():
            result[k] = translator.execute("ru", v)

        return jsonify(result)
