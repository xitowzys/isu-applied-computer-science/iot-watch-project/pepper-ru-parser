import threading
from flask import Flask
from flask_restful import Api
from loguru import logger as log
from api.routes import AddMonProduct, DeleteProduct
from api.routes.CheckProducts import CheckProducts
from config import FLASK_HOST, FLASK_PORT, FLASK_DEBUG

app: Flask = Flask(__name__)
api = Api(app)

app.config['JSON_AS_ASCII'] = False

def init_api_routes():
    api.add_resource(AddMonProduct, '/api/add-mon-product')
    api.add_resource(CheckProducts, '/api/check-products')
    api.add_resource(DeleteProduct, '/api/delete-product/<product>')
    log.success("API успешно проициализирован")


def init():
    init_api_routes()
    thread = threading.Thread(target=lambda: app.run(debug=FLASK_DEBUG, use_reloader=False, port=FLASK_PORT, host=FLASK_HOST))
    thread.daemon = True 
    thread.start()
    # , ssl_context=('ssl-keys/cert.pem', 'ssl-keys/key.pem')
